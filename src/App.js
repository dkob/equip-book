import React, {Component} from 'react';
import axios from 'axios';
import './App.css';
import {sortBy} from 'lodash';

const DEFAULT_QUERY = 'redux';
const DEFAULT_HPP = '5';
const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';
const PARAM_PAGE = 'page=';
const PARAM_HPP = 'hitsPerPage=';

const SORTS = {
    NONE: list => list,
    TITLE: list => sortBy(list, 'title'),
    AUTHOR: list => sortBy(list, 'author'),
    COMMENTS: list => sortBy(list, 'num_comments').reverse(),
    POINTS: list => sortBy(list, 'points').reverse(),
};

const updateSearchTopStoriesState = (hits, page) => (prevState) => {
    const {searchKey, results} = prevState;
    const oldHits = results && results[searchKey]
        ? results[searchKey].hits
        : [];
    const updatedHits = [
        ...oldHits,
        ...hits
    ];
    return {
        results: {
            ...results,
            [searchKey]: {hits: updatedHits, page}
        },
        isLoading: false
    };
};

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            results: null,
            searchKey: '',
            searchTerm: DEFAULT_QUERY,
            error: null,
            isLoading: false,
        };
        this.needsToSearchTopStories = this.needsToSearchTopStories.bind(this);
        this.setSearchTopStories = this.setSearchTopStories.bind(this);
        this.fetchSearchTopStories = this.fetchSearchTopStories.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this.onSearchSubmit = this.onSearchSubmit.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    needsToSearchTopStories(searchTerm) {
        return !this.state.results[searchTerm];
    }

    setSearchTopStories(result) {
        const {hits, page} = result;
        console.log("setSearchTopStories" + page + " " + hits);
        this.setState(updateSearchTopStoriesState(hits, page));
    }

    /*    fetchSearchTopStories(searchTerm, page = 0) {
            fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`)
                .then(response => response.json())
                .then(result => this.setSearchTopStories(result))
                .catch(error => this.setState({error}));
        }*/

    fetchSearchTopStories(searchTerm, page = 0) {
        this.setState({isLoading: true});

        axios(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`)
            .then(result => this.setSearchTopStories(result.data))
            .catch(error => this.setState({error}));
    }

    componentDidMount() {
        console.log("componentDidMount")
        const {searchTerm} = this.state;
        this.setState({searchKey: searchTerm});
        this.fetchSearchTopStories(searchTerm);
    }

    onDelete(objectID) {
        console.log('delete ' + objectID);
        const {searchKey, results} = this.state;
        const {hits, page} = results[searchKey];

        const isNotId = item => item.objectID !== objectID;
        const updatedHits = hits.filter(isNotId);

        this.setState({
            results: {
                ...results,
                [searchKey]: {hits: updatedHits, page}
            }
        });
    }

    onSearchChange(event) {
        this.setState({searchTerm: event.target.value});
    }

    onSearchSubmit(event) {
        console.log("onSearchSubmit")
        const {searchTerm} = this.state;
        this.setState({searchKey: searchTerm});
        if (this.needsToSearchTopStories(searchTerm)) {
            this.fetchSearchTopStories(searchTerm);
        }
        event.preventDefault();
    }

    render() {
        console.log("Rendering APP");
        const {searchTerm, results, searchKey, error, isLoading} = this.state;
        const page = (results && results[searchKey] && results[searchKey].page) || 0;
        const list = (results && results[searchKey] && results[searchKey].hits) || [];
        console.log("Searchterm " + searchTerm);
        console.log("Results " + results);
        console.log("searchKey " + searchKey);
        console.log("page " + page);
        console.log("list " + list);
        console.log("========");
        return (
            <div className="page">
                <div className="interactions">
                    <Search value={searchTerm} onChange={this.onSearchChange}
                            onSubmit={this.onSearchSubmit}>Search</Search>
                </div>
                {error
                    ? <div className="interactions">
                        <p>Something went wrong.</p>
                    </div>
                    : <Table list={list} onDelete={this.onDelete}/>}
                <div className="interactions">
                    {isLoading
                        ? <Loading/>
                        : <Button
                            onClick={() => this.fetchSearchTopStories(searchKey, page + 1)}
                        >
                            More
                        </Button>
                    }
                </div>
            </div>
        );
    }
}

class Search extends Component {
    componentDidMount() {
        if (this.input) {
            this.input.focus();
        }
    }

    render() {
        const {value, onChange, onSubmit, children} = this.props;
        return (
            <form onSubmit={onSubmit}>
                <input
                    type="text"
                    value={value}
                    onChange={onChange}
                    ref={el => this.input = el}
                />
                <button type="submit">
                    {children}
                </button>
            </form>
        );
    }
}

const Button = ({onClick, className = '', children}) =>
    <button onClick={onClick} className={className} type="button">
        {children}
    </button>;

class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortKey: 'NONE',
            isSortReverse: false,
        };
        this.onSort = this.onSort.bind(this);
    }

    onSort(sortKey) {
        const isSortReverse = this.state.sortKey === sortKey &&
            !this.state.isSortReverse;
        this.setState({sortKey, isSortReverse});
    }

    render() {
        const {list, onDelete} = this.props;
        const {sortKey, isSortReverse} = this.state;
        const sortedList = SORTS[sortKey](list);
        const reverseSortedList = isSortReverse
            ? sortedList.reverse()
            : sortedList;
        return (<div className="table">
                <div className="table-header">
                    <span style={{width: '40%'}}>
                        <Sort sortKey={'TITLE'} onSort={this.onSort} activeSortKey={sortKey}>
                            Title
                        </Sort>
                    </span>
                    <span style={{width: '30%'}}>
                        <Sort sortKey={'AUTHOR'} onSort={this.onSort} activeSortKey={sortKey}>
                            Author
                        </Sort>
                    </span>
                    <span style={{width: '10%'}}>
                        <Sort sortKey={'COMMENTS'} onSort={this.onSort} activeSortKey={sortKey}>
                            Comments
                        </Sort>
                    </span>
                    <span style={{width: '10%'}}>
                        <Sort sortKey={'POINTS'} onSort={this.onSort} activeSortKey={sortKey}>
                            Points
                        </Sort>
                    </span>
                    <span style={{width: '10%'}}>
                        Archive
                    </span>
                </div>

                {reverseSortedList.map(item =>
                        <div key={item.objectID} className="table-row">
                            <span style={largeColumn}>
                                <a href={item.url}>{item.title}</a>
                            </span>
                            <span style={midColumn}>{item.author}</span>
                            <span style={smallColumn}>{item.num_comments}</span>
                            <span style={smallColumn}>{item.points}</span>
                            <span>
                    <Button className="button-inline" onClick={() => onDelete(item.objectID)}>
                        Delete
                    </Button>
                </span>
                        </div>
                )}
            </div>
        );
    }
}

const Loading = () =>
    <div>Loading ...</div>

const Sort = ({sortKey, activeSortKey, onSort, children}) => {
    const sortClass = ['button-inline'];
    if (sortKey === activeSortKey) {
        sortClass.push('button-active');
    }
    return (
        <Button
            onClick={() => onSort(sortKey)}
            className={sortClass.join(' ')}
        >
            {children}
        </Button>
    );
}

const largeColumn = {
    width: '40%',
};
const midColumn = {
    width: '30%',
};
const smallColumn = {
    width: '10%',
};

/*function isSearched(searchTerm) {
    return function (item) {
        return item.title.toLowerCase().includes(searchTerm.toLowerCase());
    }
}

const isSearched = searchTerm => item =>
    item.title.toLowerCase().includes(searchTerm.toLowerCase());*/

export default App;

export {
    Button,
    Search,
    Table
};